import arcade
from game.graphics import MyGame

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600


def main():
    game = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    game.setup()
    arcade.run()


main()
