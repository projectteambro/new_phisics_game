import arcade

from game.logic import Logic


class MyGame(arcade.Window):

    def init(self, width, height):
        super().init(width, height)

    def setup(self):
        arcade.set_background_color(arcade.color.WHITE)

        self.logic = Logic()
        self.sticks = arcade.SpriteList()
        self.core = arcade.Sprite()
        self.update_sprites()
        self.logic.load_stick()

    def update_sprites(self):
        self.sticks = arcade.SpriteList()

        for stick in self.logic.list_sticks:
            sprite = arcade.Sprite(stick.imagePath)
            sprite.angle = stick.angle
            sprite.center_x = stick.x
            sprite.center_y = stick.y

            self.sticks.append(sprite)
        self.logic.update_stick()

        self.gun = arcade.Sprite(self.logic.gun.imagePath)
        self.gun.center_x = self.logic.gun.x
        self.gun.center_y = self.logic.gun.y
        self.gun.angle = self.logic.gun.angle

        if self.logic.core is not None:

            if self.logic.core.x >= 800 or self.logic.core.x < 0 or self.logic.core.y >= 600 or \
                    self.logic.core.y < 0 or self.logic.core.speed == 0:
                self.core = arcade.Sprite()
                self.logic.core = None
            else:
                self.core = arcade.Sprite(self.logic.core.imagePath)
                self.logic.core.update_coord()

                self.core.center_x = self.logic.core.x
                self.core.center_y = self.logic.core.y

            count = 0
            for stick in self.sticks:
                if arcade.check_for_collision(self.core, stick):
                    self.logic.collision(count)
                    return
                count += 1

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        self.sticks.draw()
        self.gun.draw()

        if self.logic.core is not None:
            self.core.draw()

    def update(self, delta_time):
        self.update_sprites()

    def on_key_press(self, key, modifiers):
        self.logic.check_key(key)
