import math


class Core:
    def __init__(self, angle):
        self.imagePath = "image/core.png"
        self.x = 70
        self.y = 45
        self.angle = angle
        self.speed = 200
        self.count = 0
        self.mass = 10
        self.collision = False
        self.radAngle = math.radians(self.angle)
        self.delta_t = 0.055
        self.addX = 70
        self.addY = 55
        self.damping = 2

    def update_coord(self):
        self.x = self.speed * self.count * self.delta_t * math.cos(self.radAngle) + self.addX
        self.y = (self.speed * self.count * self.delta_t * math.sin(self.radAngle)) - (9.8 * self.count * self.mass /
                                                                                       self.damping * self.delta_t *
                                                                                       self.count * (self.delta_t / 2)
                                                                                       ) + self.addY

        self.count += 1

    def update_angle(self, ang):
        self.angle += ang
        self.radAngle = math.radians(self.angle)
        self.addX = self.x
        self.addY = self.y
        self.count = 1
        self.speed -= 30


class Stick:
    def __init__(self, x, y, angle):
        self.imagePath = "image/horisontal_stick.png"
        self.x = x
        self.y = y
        self.angle = angle

        self.mass = 200
        self.len = 10
        self.radAngle = math.radians(self.angle)
        self.delta_t = 0.000005
        self.count = 1
        self.new_angle1 = 0

        self.speed = 200
        self.collision = False
        self.addX = self.x
        self.addY = self.y
        self.damping = 2
        self.len_up = 0
        self.collision_distance = 9999

        self.mass_ball = 1
        self.vel_ball = 1
        self.momentIn = 0
        self.impulse = 0
        self.new_angle = 0

    def update_coord(self, core):
        self.momentIn = ((1 / 12) * (self.len * self.len * self.mass))
        self.impulse = 2 * self.mass_ball * self.speed
        self.new_angle = self.momentIn / self.impulse
        self.angle += self.new_angle

        x = self.x
        y = self.y

        y = -(self.speed * self.count * self.delta_t * math.sin(self.radAngle)) - (9.8 * self.count * self.mass
                                                                                   * self.delta_t * 100 *
                                                                                   self.count * (self.delta_t / 2)
                                                                                   ) + self.addY
        x = (self.speed * self.mass * self.count * self.delta_t * math.cos(self.new_angle1)) * 50 + self.addX

        if y > 0:
            self.x = x
            self.y = y
            self.count += 1

    def distance_collision(self, core):
        self.collision_distance = core.y - self.y

    def search_new_speed(self, core):
        self.count = 1
        self.addX = self.x
        self.addY = self.y

        v_stick = ((core.mass - self.mass) * core.speed) / (core.mass + self.mass)
        v_core = (2 * core.mass * core.speed) / (core.mass + self.mass)

        core.speed = v_core
        self.speed = v_stick
        self.collision_distance = 9999

        self.addX = self.x
        self.addY = self.y

        self.new_angle1 = -core.angle


class Gun:
    def __init__(self):
        self.imagePath = "image/gun.png"
        self.x = 50
        self.y = 35
        self.angle = 1
