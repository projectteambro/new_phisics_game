import arcade
import game.model as model
from game.levels import levels


class Logic:

    def __init__(self):
        self.list_sticks = []
        self.core = None
        self.gun = model.Gun()

    def check_key(self, key):
        if key == arcade.key.W:
            if self.gun.angle + 2 <= 31:
                self.gun.angle += 2
        elif key == arcade.key.S:
            if self.gun.angle - 2 >= 1:
                self.gun.angle -= 2
        elif key == arcade.key.SPACE:
            if self.core is not None:
                return
            self.core = model.Core(self.gun.angle)
        elif key == arcade.key.X:
            self.core = None
        elif key == arcade.key.C:
            self.list_sticks=[]
            self.load_stick()

    def load_stick(self):
        lvl = levels.lvl1

        coord_x = 250
        coord_y = 170
        for a in lvl:
            for b in a:
                if b == "|":
                    self.list_sticks.append(model.Stick(800 - coord_x, coord_y, 90))

                elif b == "_":
                    self.list_sticks.append(model.Stick(800 - coord_x, coord_y, 0))

                elif b == "\\":
                    self.list_sticks.append(model.Stick(800 - coord_x, coord_y, 120))

                elif b == "/":
                    self.list_sticks.append(model.Stick(800 - coord_x, coord_y, 60))

                coord_x -= 35
            coord_y -= 35
            coord_x = 250

    def update_stick(self):
        for stick in self.list_sticks:
            if stick.collision:
                stick.update_coord(self.core)

    def collision(self, count):
        self.list_sticks[count].distance_collision(self.core)
        self.core.angle = self.list_sticks[count].angle
        self.core.update_angle(self.list_sticks[count].angle)
        self.list_sticks[count].search_new_speed(self.core)
        self.list_sticks[count].collision = True
